# Credits

* [pass - the standard unix password manager](https://www.passwordstore.org/)
   was written by Jason A. Donenfeld of [zx2c4.com](https://www.zx2c4.com/)
   with contributions from a loving community.

   The `pass team` extension is also inspired by Jason:
   > Multiple GPG keys can be specified, for using pass in a team setting, and
   different folders can have different GPG keys, by using -p.

   This quote from the [pass website](https://www.passwordstore.org/) got this
   project going.
* [IndiScale](https://www.indiscale.com) supported this project with their
  time, feedback and other resources.
