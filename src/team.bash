#!/usr/bin/env bash
#
# Copyright (C) 2021-2022 Timm Fitschen <t.fitschen@indiscale.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
set -o nounset # exit when undeclared variables are being used
set -o errexit # exit when a command fails

PASS_TEAM_VERSION='0.1.0-rc.11'
__BACKUP_IFS=${IFS:-}
PROGRAM=${PROGRAM:-pass}
PASSWORD_STORE_TEAM_DIR=${PASSWORD_STORE_TEAM_DIR:-.team}
ROLE_DIR=${PASSWORD_STORE_TEAM_DIR}/role
SHARED_HINT_FILE=".team.shared"
PASSWORD_STORE_TEAM_DEBUG_LEVEL=${PASSWORD_STORE_TEAM_DEBUG_LEVEL:-0}
GPG="gpg"

############
# Exit Codes
############
EXIT_GENERIC_ERROR=1
EXIT_USAGE_ERROR=81
EXIT_PASS_NOT_INITIALIZED=82
EXIT_PASS_TEAM_NOT_INITIALIZED=83
EXIT_UNKNOWN_GPG_PUBLIC_KEY=84
EXIT_ROLE_IS_ASSIGNED_TO_SUBJECTS=85
EXIT_ROLE_DOES_NOT_EXIST=86
EXIT_ROLE_DOES_EXIST=87
EXIT_CANNOT_UNASSIGN_SUBJECT_NOT_ASSIGNED=88
EXIT_DIRECTORY_DOES_NOT_EXIST=89
EXIT_NOT_A_DIRECTORY=90
EXIT_NOT_A_SHARED_DIRECTORY=91
EXIT_BAD_ROLE_NAME_DOT_START=92
EXIT_BAD_ROLE_NAME_NO_LATIN_CHAR=93
EXIT_BAD_ROLE_NAME_UNALLOWED_CHAR=94

_COLOR_RED='\033[01;31m'
_COLOR_BLUE='\033[01;34m'
_COLOR_YELL='\033[01;33m'
_COLOR_NONE='\033[00m'

set_no_color () {
    _COLOR_RED=''
    _COLOR_BLUE=''
    _COLOR_NONE=''
    _COLOR_YELL=''
}

#shellcheck disable=SC2236
if [ ! -z ${NO_COLOR+x} ] \
    || [ ! -z ${PASSWORD_STORE_NO_COLOR+x} ] \
    || [ ! -z ${PASSWORD_STORE_TEAM_NO_COLOR+x} ] \
    || [ "$TERM" = "dumb" ] ; then
    set_no_color
fi

_color_blue () {
    printf "$_COLOR_BLUE%s$_COLOR_NONE" "$1"
}

_color_red () {
    printf "$_COLOR_RED%s$_COLOR_NONE" "$1"
}

_color_yellow () {
    printf "$_COLOR_YELL%s$_COLOR_NONE" "$1"
}

PASS_TEAM_QUIET=0
set_quiet () {
    set_no_color
    PASS_TEAM_QUIET=1
}

is_quiet () {
    [ "$PASS_TEAM_QUIET" -eq "1" ] && return 0
    return 1
}

#shellcheck disable=SC2236
if [ ! -z ${PASSWORD_STORE_TEAM_QUIET+x} ] ; then
    set_quiet
fi

warn () {
    is_quiet || echo -e "$(_color_yellow "Warning:") $1" | awk 'NR>1{print "         " $0} NR==1{print}' 1>&2
}

error_exit () {
    is_quiet || echo -e "$(_color_red "Error:") ${1:-An error has occurred.}" | awk 'NR>1{print "       " $0} NR==1{print}' 1>&2
    exit "${2:-$EXIT_GENERIC_ERROR}"
}

usage_error_exit () {
    error_exit "$1\\n\\nUsage:\\n$PROGRAM team $2\\n\\nGet Help:\\n$PROGRAM team --help" $EXIT_USAGE_ERROR
}


__debug () {
    local level="$1"
    [ "$PASSWORD_STORE_TEAM_DEBUG_LEVEL" -ge "$level" ] && echo "[DEBUG-$level] $2" 1>&2
}

cmd_team_version() {
    case "${1:-}" in
        -q|--quiet)
            set_quiet
            ;;
    esac

    if is_quiet ; then
        echo "$PASS_TEAM_VERSION"
        return 0;
    fi

    local padded_version_string="v$PASS_TEAM_VERSION"
    while [ "24" -gt "${#padded_version_string}" ] ; do
        padded_version_string="${padded_version_string} " ;
        [ "24" -gt "${#padded_version_string}" ] && padded_version_string=" ${padded_version_string}" ;
    done
	cat <<-_EOF
	============================================
	=    pass team: team extension for pass    =
	=                                          =
	=         ${padded_version_string}         =
	=                                          =
	=             Timm C. Fitschen             =
	=         t.fitschen@indiscale.com         =
	=                                          =
	============================================
	= pass team is free software. Print the    =
	= license with "pass team --license"       =
	============================================
	= For the original pass, see:              =
	=            http://www.passwordstore.org/ =
	============================================
	_EOF
}

cmd_team_license () {
    cat <<-_EOF
	============================================
	= pass team is licensed under              =
	=                                          =
	=              GPLv3 or later              =
	=                                          =
	= This program is free software: you can   =
	= redistribute it and/or modify it under   =
	= the terms of the GNU General Public      =
	= License as published by the Free         =
	= Software Foundation, either version 3 of =
	= the License, or (at your option) any     =
	= later version.                           =
	=                                          =
	= This program is distributed in the hope  =
	= that it will be useful, but WITHOUT ANY  =
	= WARRANTY; without even the implied       =
	= warranty of MERCHANTABILITY or FITNESS   =
	= FOR A PARTICULAR PURPOSE.  See the GNU   =
	= General Public License for more details. =
	=                                          =
	= You should have received a copy of the   =
	= GNU General Public License along with    =
	= this program.  If not, see               =
	= <https://www.gnu.org/licenses/>.         =
	============================================
	_EOF
}

role_usage_details() {
    cat <<-_EOF
	    $PROGRAM team role COMMAND...

	Commands:
	    ls|list
	        List roles.
	    show <role-name>...
	        Show the role and the assigned gpg-ids.
	    create <role-name>...
	        Create a new role in the team.
	    remove <role-name>...
	        Remove the role from the team
	    assign <role-name> <gpg-id>...
	        Assign the given role to all gpg-ids.
	    unassign <role-name> <gpg-id>...
	        Unassign the given role from all gpg-ids.
	    help|--help
	        Show this help.
	_EOF
}

cmd_role_usage() {
    cmd_team_version
    cat <<-_EOF

	Usage:
	$(role_usage_details)
	_EOF
}

share_usage_details() {
    SHARE="$PROGRAM team share"
    cat <<-_EOF
	    $SHARE COMMAND...

	Commands:
	    ls|list
	        List all shared directories.
	    show <directory>
	        Show all roles which have access to the given directory.
	    add <directory> <role>[...]
	        Add the roles to the shared directory. Same as '$SHARE set' if the
	        directory has not been shared previously.
	    set <directory> <role>[...]
	        Add the directory to the list of shared directories and share it
	        with all of the given roles and remove any other roles from this
	        share.
	    reencrypt (--all|<directory>...)
	        Reencrypt the given directories. This command is useful after an
	        administrator changed the role assignments or the shared directory
	        and yet could not reencrypt the shared passwords accordingly
	        because they were lacking the read permission for the shared
	        directory.
	    remove <directory> (--all|<role>[...])
	        Remove the roles from the shared directory. '--all' removes the
	        shared directory completely.
	        If the directory is not being shared with any role anymore, the
	        directory is removed from the list of shared directories and all
	        passwords are reencrypted for the local password store. 
	    unset <directory>
	        Alias for '$SHARE remove <directory> --all'.
	    help|--help
	        Show this help.
	_EOF
}

cmd_share_usage() {
    cmd_team_version
    cat <<-_EOF

	Usage:
	$(share_usage_details)
	_EOF
}

cmd_team_usage() {
    local exit_code=0
    case "${1:-}" in
        role) shift;                cmd_role_usage "$@" ;;
        share) shift;               cmd_share_usage "$@" ;;
        --exit) shift;              exit_code="$1" ;;
        --no-color) shift;          set_no_color ; cmd_team_usage "$@" ;;
    esac

    cmd_team_version

    TEAM="$PROGRAM team"
    cat <<-_EOF

	Usage:
	    $TEAM [OPTIONS...] COMMAND...

	Options:
	    --help|-h
	        Show this help and exit. When combined with a COMMAND a more
	        detailed help is printed.
	    --licence|--version|--no-color|--debug|-d|--quiet|-q|
	        See \`man pass-team\` for more information.

	Commands:
	    init ([--trusted] <gpg-id>)+
	        Intialize the pass team extensions and use the given gpg-ids
	        as pass team managers. The pass team managers are responsible for
	        managing the roles and shared directories. Managers who are
	        'trusted' will also be added to every share (and thus will be able
	        to read every password). This is handy when passwords need to be
	        reencrypted often because of changing roles or shares.
	    role [...]
	        Commands for creating, removing, assigning role and more. Execute
	            '$TEAM role help'
	        for more information.
	    share [...]
	        Commands for adding, removing, listing and reencrypting shared
	        password directories. Execute
	            '$TEAM share help'
	        for more information.
	_EOF
    exit "$exit_code"
}

is_known_public_key() {
    [ -z "$1" ] && return 1
    $GPG -k "$1" &> /dev/null || return 1
    return 0
}

expect_is_known_pubkey() {
    is_known_public_key "$1" || error_exit "The public key of '$1' is unknown.\
        \\nPlease add it to your gpg keyring." "$EXIT_UNKNOWN_GPG_PUBLIC_KEY"
}

get_fingerprint() {
    $GPG -k --with-colons "$1" 2>/dev/null | awk -F: '$1 == "fpr" {print $10;}' | head -n1
}

is_pass_initialized () {
    [ -f "$PREFIX/.gpg-id" ] || return 1
}

expect_pass_initialized () {
    is_pass_initialized || error_exit "Pass is not initialized. You must run:\
        \\n    $PROGRAM init <gpg-id>...\
        \\nbefore you may use the password store." "$EXIT_PASS_NOT_INITIALIZED"
}

cmd_team_init() {
    [[ -z "${1:-}" ]] && usage_error_exit "Missing <gpg-id> argument." "init ([--trusted] <gpg-id>)+"
    case "${1:-}" in
        help|--help|-h)
            shift;
            cmd_team_usage "$@"
            ;;
    esac
    expect_pass_initialized

    local normal_mangers=()
    local trusted_managers=()
    local is_trusted=0
    for arg in "$@"; do
        case "$arg" in
            --trusted)
                is_trusted=1
                ;;
            -*) usage_error_exit "Invalid argument '$arg'" "init ([--trusted] <gpg-id>)+"
                ;;
            *)
                expect_is_known_pubkey "$arg"
                local finger_print
                finger_print=$(get_fingerprint "$arg")
                normal_mangers+=("$finger_print")
                [ "$is_trusted" -eq "1" ] && trusted_managers+=("$finger_print")
                is_trusted=0
                ;;
        esac
    done

    set +o nounset
    cmd_init "--path=$PASSWORD_STORE_TEAM_DIR" "${normal_mangers[@]}"
    set -o nounset
    local trusted_gpgid_file="$PREFIX/$PASSWORD_STORE_TEAM_DIR/.trusted-gpg-id" trusted
    [ -f "$trusted_gpgid_file" ] && rm "$trusted_gpgid_file"
    for trusted in "${trusted_managers[@]}" ; do
        echo "$trusted" >> "$trusted_gpgid_file"
    done
    mkdir -p "$PREFIX/$ROLE_DIR"

    update_shares

    # TODO GIT
}

expect_initialized () {
    is_initialized || error_exit "Pass team is not initialized. You must run:\
        \\n    $PROGRAM team init <gpg-id>...\
        \\nbefore you may use the password store." "$EXIT_PASS_TEAM_NOT_INITIALIZED"
}

is_initialized() {
    [[ ! -f "$PREFIX/$PASSWORD_STORE_TEAM_DIR/.gpg-id" ]] && return 1
    return 0
}

is_role () {
    local role_file="${PREFIX}/${ROLE_DIR}/${1}"
    [ -f "$role_file" ] || return 1
}

check_role_name_ok () {
    local role="${1:-}"
    local usage="${2:-"\\n\\nUsage:\\n    $PROGRAM team role create <role>"}"
    [ -z "$role" ] && echo -e "Role name is missing. $usage" && return $EXIT_USAGE_ERROR
    [ -z "$(echo "$role" | sed 's/^\..*$//' | sed 's/^.*\.$"//')" ] && echo -e "Role names must not start or end with a dot." && return "$EXIT_BAD_ROLE_NAME_DOT_START"
    [ "${role//[a-zA-Z]/}" = "$role" ] && echo "Role names must contain at least one character from the latin alphabet." && return "$EXIT_BAD_ROLE_NAME_NO_LATIN_CHAR"
    [ -n "${role//[-_.a-zA-Z0-9]/}" ] && echo "Role names must not contain any other characters than latin alphabetic characters, arabic numbers, '-', '_', and '.'." && return "$EXIT_BAD_ROLE_NAME_UNALLOWED_CHAR"
    return 0
}

cmd_role_create() {
    expect_initialized
    local role
    __debug "2" "Attempt to create roles $*"
    for role in "$@" ; do
        check_role_name_ok "$role" 1>&2
        local role_name_exit_code="$?"
        [ "$role_name_exit_code" -eq "0" ] || exit $role_name_exit_code

        is_role "$role" && error_exit "The role '$role' does already exist." "$EXIT_ROLE_DOES_EXIST"
    done

    for role in "$@" ; do
        __debug "1" "Create role $role"
        local role_file="${PREFIX}/${ROLE_DIR}/${role}"
        touch "$role_file"
        is_quiet || echo "Created role '$role'."
    done

    # TODO GIT
}

cmd_role_show() {
    expect_initialized

    # no role given, print known roles
    [[ -z "${1:-}" ]] && usage_error_exit "Missing <role> argument. Did you mean '$PROGRAM team role list'?" "role show <role>..."

    local role
    for role in "$@" ; do

        expect_is_role "$role"

        local role_file="${PREFIX}/${ROLE_DIR}/${role}"
        echo -e "Role: $(_color_blue "$role")"
        echo "└── GPG Keys:"
        print_nice_keys "$role_file" | awk "{print \"    ├── \" \$0 }" | sed '$s/├──/└──/'
    done
}

cmd_role_list() {
    local role_dir="${PREFIX}/${ROLE_DIR}"
    if is_quiet ; then
        find "$role_dir" -type f -printf '%f\n' | \
            sort | sed ':a; N; $!ba; s/\n/ /g'
    else
        echo "Roles"
        tree -C -l --noreport "$role_dir" | tail -n +2
    fi
}

is_role_assigned() {
    local role_file="${PREFIX}/${ROLE_DIR}/${1}"
    if [ -s "$role_file" ] ; then
        return 0 ;
    fi
    return 1
}

expect_role_is_not_assigned() {
    local details=""
    [ -n "${2:-}" ] && details="\\n$2"
    is_role_assigned "$1" && error_exit "The role is assigned to subjects.$details" "$EXIT_ROLE_IS_ASSIGNED_TO_SUBJECTS"
}

cmd_role_rm() {
    local role
    for role in "$@" ; do
        expect_is_role "$role"

        expect_role_is_not_assigned "$role" "Please unassign all subjects from this role before deleting it."
    done

    for role in "$@" ; do
        expect_is_role "$role"
        local role_file="${PREFIX}/${ROLE_DIR}/${role}"
        rm "$role_file"
        is_quiet || echo "Removed role '$role'."
    done

    # TODO GIT
}

role_assign_single() {
    local role_file="$1"
    local finger_print="$2"

    role_file_temp="${role_file}.tmp.${RANDOM}.${RANDOM}.${RANDOM}.${RANDOM}.--"
    cp "$role_file" "$role_file_temp"
    echo "$finger_print" >> "$role_file_temp"
    sort -o "$role_file" "$role_file_temp"
    rm "$role_file_temp"
    # TODO GIT
}

role_unassign_single() {
    __debug "2" "Attempt to unassign $2 from ${1#"$PREFIX/$ROLE_DIR/"}"
    local role_file="$1"
    local gid="$2"
    if is_line_in_file "$role_file" "$gid" ; then
        sed "/$gid/d" -i "$role_file"
        return 0
    elif is_known_public_key "$gid" ; then
        local finger_print
        finger_print=$(get_fingerprint "$gid")
        is_line_in_file "$role_file" "$finger_print" || error_exit "Cannot unassign the subject '$gid' from the role '${role_file#"$PREFIX/$ROLE_DIR/"}'\\nbecause it is not assigned." $EXIT_CANNOT_UNASSIGN_SUBJECT_NOT_ASSIGNED
        sed "/$finger_print/d" -i "$role_file"
        return 0
    fi

    error_exit "Cannot unassign the subject '$gid'\\n because its public key is unknown." $EXIT_UNKNOWN_GPG_PUBLIC_KEY
}

is_line_in_file() {
    set -o noglob
    local file=
    file="$(realpath "$1")"
    local search_line=$2
    while IFS= read -r next_line; do
        [[ "$next_line" == "$search_line" ]] && return 0
    done < "$file"
    set +o noglob

    # not found
    return 1
}

is_shared_dir () {
    local shared_dir="${1#"$PREFIX/"}"
    local shared_hint_file="${PREFIX}/${shared_dir}/${SHARED_HINT_FILE}"
    [ -f "$shared_hint_file" ] || return 1
}

expect_is_shared_dir() {
    __debug "3" "expect_is_shared_dir $1"
    is_shared_dir "$1" || error_exit "'${1#"$PREFIX/"}' is not a shared directory." $EXIT_NOT_A_SHARED_DIRECTORY
}

expect_is_role() {
    local details=""
    [ -n "${2:-}" ] && details="\\n$2"
    if ! is_role "$1" ; then
        error_exit "The role '$1' does not exist.$details" $EXIT_ROLE_DOES_NOT_EXIST
    fi
}

cmd_role_assign() {
    expect_is_role "$1" "Please add it to your pass team."
    local role="$1"; shift
    local subjects=("$@")
    [ -z "${subjects[*]}" ] && usage_error_exit "Missing <gpg-id> argument." "role assign <role> <gpg-id>..."
    local role_file="${PREFIX}/${ROLE_DIR}/${role}"
    for gpg_id in "${subjects[@]}"; do

        # easy way to implement nested roles?
        #is_role $gpg_id || expect_is_known_pubkey "$gpg_id"

        expect_is_known_pubkey "$gpg_id"
        local fingerprint
        fingerprint="$(get_fingerprint "$gpg_id")"

        # check if this fingerprint is already present in the role's file
        [ -f "$role_file" ] && is_line_in_file "$role_file" "$fingerprint" \
            && { is_quiet || echo "Subject '$gpg_id' has already been assigned to role '$role'. Ignoring." ; } \
            && continue
        role_assign_single "$role_file" "$fingerprint"
        is_quiet || echo "Assigned role '$role' to subject '$gpg_id'."
    done
    update_shares "$role"
    # TODO GIT
}

cmd_role_unassign() {
    local role="$1"
    expect_is_role "$role"
    local role_file="${PREFIX}/${ROLE_DIR}/${role}"
    shift
    local subjects=("$@")
    for gpg_id in "${subjects[@]}"; do
        role_unassign_single "$role_file" "$gpg_id"
        is_quiet || echo "Unassigned role '$role' from subject '$gpg_id'."
    done
    update_shares "$role"
    # TODO GIT
}


cmd_team_role() {
    case "${1:-}" in
        show) shift;                cmd_role_show "$@" ;;
        ls|list) shift;             cmd_role_list "$@" ;;
        create|add|insert) shift;   cmd_role_create "$@" ;;
        delete|remove|rm) shift;    cmd_role_rm "$@" ;;
        assign) shift;              cmd_role_assign "$@" ;;
        unassign) shift;            cmd_role_unassign "$@" ;;
        help|--help) shift;         cmd_role_usage "$@" ;;
        *)                          cmd_role_usage "$@" ;;
    esac
}

resolve_gpgids () {
    __debug "3" "Resolve gpg fingerprints for role $1"
    [ -z "$1" ] && return 0
    local role_file="${PREFIX}/${ROLE_DIR}/${1}"
    [ ! -f "$role_file" ] && error_exit "The role '$1' does not exist." $EXIT_ROLE_DOES_NOT_EXIST
    local result=()
    while IFS= read -r member; do
        # easy way to implement nested roles?
        # { is_role "$member" && resolve_gpgids "$member" ; } || result+=("$member")
        result+=("$member")
    done < "$role_file"
    IFS=$'\n'
    echo "${result[*]}"
    IFS="$__BACKUP_IFS"
}

list_shared_hint_files () {
    find "$PREFIX" -name "${SHARED_HINT_FILE}" -printf "%h/%f "
}

share_with_trusted_team_managers () {
    expect_initialized
    local gpgid_file="$1"
    local trusted="$PREFIX/$PASSWORD_STORE_TEAM_DIR/.trusted-gpg-id"
    [ -f "$trusted" ] && cat "$trusted" >> "$gpgid_file"
}

is_dir_empty () {
    local num_of_files
    num_of_files="$(find "$1" -maxdepth 1 -mindepth 1 | wc -l)"
    [ "$num_of_files" -eq "0" ] || return 1
}

delete_empty_parents () {
    local dir parent
    dir="$(realpath "$1")"
    parent="$(dirname "$dir")"
    if [ "$dir" = "$PREFIX" ] ; then
        __debug "3" "not deleting $dir"
        return 0
    fi

    __debug "3" "deleting empty dir $dir"
    rm -r "$dir"
    is_dir_empty "$parent" && delete_empty_parents "$parent"
}

update_single_share () {
    __debug "2" "Update single share ${1#"$PREFIX/"} ${2:-}"
    local shared_dir="$1"

    # for_role just hints which of the roles had been changed (if any). We use
    # it for keeping the output shorter and easier to read.
    local for_role="${2:-}"

    expect_is_shared_dir "$shared_dir"
    local gpgid_file="$shared_dir/.gpg-id"
    local shared_hint_file="$shared_dir/$SHARED_HINT_FILE"

    if [ -s "$shared_hint_file" ] ; then
        local shared_roles
        shared_roles="$(sed -z 's/\n/ /g;s/ $//' "$shared_hint_file")"

        gpgid_file_temp="${gpgid_file}.tmp.${RANDOM}.${RANDOM}.${RANDOM}.${RANDOM}.--"
        touch "$gpgid_file_temp"
        local found_role="$for_role"
        for role in ${shared_roles} ; do
            if [ "$role" = "$for_role" ] ; then
                found_role=""
            fi
            local gpgids
            readarray -d $'\n' -t gpgids < <(resolve_gpgids "$role")
            for id in "${gpgids[@]}" ; do
                is_line_in_file "$gpgid_file_temp" "$id" && continue
                echo "$id" >> "$gpgid_file_temp"
            done
        done
        is_quiet || { [ -z "$found_role" ] && echo "'${shared_dir#"$PREFIX/"}' is shared with: ${shared_roles// /, }." ; }
        share_with_trusted_team_managers "$gpgid_file_temp"
        sort -o "$gpgid_file" "$gpgid_file_temp"
        rm "$gpgid_file_temp"
    else
        [ -f "$shared_hint_file" ] && rm "$shared_hint_file"
        [ -f "$gpgid_file" ] && rm "$gpgid_file"
        is_quiet || echo "'${shared_dir#"$PREFIX/"}' is not shared any longer."
    fi

    if is_dir_empty "$shared_dir"; then
        delete_empty_parents "$shared_dir"
        return 0
    fi

    local reenc_out has_error
    set +o nounset
    reenc_out="$(reencrypt_path "$shared_dir" 2>&1)"
    set -o nounset

    # what's left is an error
    has_error="$(echo "$reenc_out" | sed '/reencrypting to/d')"
    if [ -n "$has_error" ] ; then
        warn "Could not reencrypt '${shared_dir#"$PREFIX/"}'.\\n$has_error"
    else
        is_quiet || echo "$reenc_out"
    fi
}



cmd_share_reencrypt () {
    [[ -z "${1:-}" ]] && usage_error_exit "Missing <directory> argument." "share update (--all|<directory>...)"
    case "${1:-}" in
        -a|--all)
            update_shares
            ;;
        *)
            for share in "$@" ; do
                expect_is_shared_dir "${PREFIX}/${share}"
            done
            for share in "$@" ; do
                update_single_share "${PREFIX}/${share}"
            done
            ;;
    esac
    # TODO GIT
}

update_shares () {
    local shared_hint_files
    local for_role="${1:-}"
    shared_hint_files="$(list_shared_hint_files)"

    for f in $shared_hint_files ; do
        local shared
        shared="${f%/*}"
        update_single_share "$shared" "$for_role"
    done
}

add_single_share () {
    [ ! -d "$1" ] && mkdir -p "$1"
    local shared_hint_file="$1/${SHARED_HINT_FILE}"
    local role="$2"
    __debug "2" "Add role $role to shared directory ${1#"$PREFIX/"}"

    [ -f "$shared_hint_file" ] || echo "$role" >> "$shared_hint_file"
    is_line_in_file "$shared_hint_file" "$role" || echo "$role" >> "$shared_hint_file"

}

expect_is_dir () {
    local directory="$1"
    [ ! -d "$PREFIX/$directory" ] && error_exit "'$directory' isn't a directory." $EXIT_NOT_A_DIRECTORY
}

expect_dir_exists () {
    local directory="$1"
    [ ! -e "$PREFIX/$directory" ] && error_exit "Directory '$directory' does not exist." $EXIT_DIRECTORY_DOES_NOT_EXIST
}

print_nice_keys () {
    local fngpr keyid uid
    while IFS= read -r fngpr ; do
        keyid="$($GPG -k --with-colons "$fngpr" |awk -F: '$1=="pub"{print $5}')"
        uid="$($GPG -k --with-colons "$fngpr" | awk -F: '$1=="uid"{print $10}' | head -n 1)"
        echo -e "$_COLOR_BLUE$keyid$_COLOR_NONE ── $_COLOR_BLUE$uid$_COLOR_NONE"
    done < "$1"
}

cmd_share_show () {
    local share="$1"

    [[ -z "$share" ]] && usage_error_exit "Missing <directory> argument." "share show <directory>..."

    local shared_hint="$PREFIX/$share/${SHARED_HINT_FILE}"
    expect_dir_exists "$share"
    expect_is_dir "$share"
    expect_is_shared_dir "$share"
    local sharedids
    sharedids="$PREFIX/$share/.gpg-id"
    echo "Share: $(_color_blue "$share")"
    echo "├── Roles:"
    awk "{print \"│   ├── $_COLOR_BLUE\" \$1 \"$_COLOR_NONE\"}" "$shared_hint" | sed '$s/├──/└──/'
    echo "└── GPG Keys:"
    print_nice_keys "$sharedids" | awk "{print \"    ├── \" \$0 }" | sed '$s/├──/└──/'

}

cmd_share_list () {
    if is_quiet ; then
        # linter in debian oldstable complains that "Backslash is literal in "\0". Prefer explicit escaping: "\\0"."
        # shellcheck disable=SC1117
        find "$PREFIX" -type f -iname "${SHARED_HINT_FILE}" -printf "%h/ \0" | sort -z | sed "s|\x0||g" | sed "s|$PREFIX/||g"
    else
        echo "Shared Directories"
        tree -C -l --noreport "$PREFIX" -a -P "${SHARED_HINT_FILE}" --prune | tail -n +2 | sed "/${SHARED_HINT_FILE}/d"
    fi
}

is_role_not_empty () {
    local role_file="${PREFIX}/${ROLE_DIR}/${1}"
    [ -s "$role_file" ] || return 1
    return 0
}

cmd_share_add () {
    local shared="$1"
    shift
    local roles=("$@")

    [ -z "$shared" ] && usage_error_exit "Missing <directory> argument." "share (add|set) <directory> <role>..."
    [ -z "${roles[*]}" ] && usage_error_exit "Missing <role> argument." "share (add|set) <directory> <role>..."

    local shared_dir="${PREFIX}/${shared}"
    [ -e "$shared_dir" ] && [ ! -d "$shared_dir" ] && error_exit "$shared isn't a directory.\\nPass team can only share directories of the password store." $EXIT_NOT_A_DIRECTORY


    for role in "${roles[@]}"; do
        expect_is_role "$role" "Please add it to your pass team."
        is_role_not_empty "$role" || warn "You are sharing a directory with a role which is not assigned to any subject".
    done

    for role in "${roles[@]}"; do
        add_single_share "$shared_dir" "$role"
    done

    local shared_hint_file="$shared_dir/${SHARED_HINT_FILE}"
    sort -o "$shared_hint_file" "$shared_hint_file"

    # TODO sign shared_hint_file
    update_single_share "$shared_dir"
}

unset_share () {
    local shared_dir="${1#"$PREFIX/"}"
    local shared_hint_file="${PREFIX}/${shared_dir}/${SHARED_HINT_FILE}"

    rm "$shared_hint_file"
    touch "$shared_hint_file"
}

remove_single_share () {
    local shared_hint_file="$1/$SHARED_HINT_FILE"
    local role=$2

    # TODO(tf) check directory is shared with role

    sed -i "/$role/d" "$shared_hint_file"
}

cmd_share_rm () {
    local shared="$1"
    [ -z "$shared" ] && usage_error_exit "Missing <directory> argument." "share remove <directory> (--all|<role>...)"

    expect_is_shared_dir "$shared"
    local shared_dir="${PREFIX}/${shared}"
    shift
    if [ "$1" = "--all" ] ; then
        shift
        unset_share "$shared";
    else
        local roles=("$@")
        [ -z "${roles[*]}" ] && usage_error_exit "Missing <role> argument." "share remove <directory> (--all|<role>...)"


        local role
        for role in "${roles[@]}"; do
            remove_single_share "$shared_dir" "$role"
        done
    fi
    update_single_share "$shared_dir"
    # TODO sign shared_hint_file

}

cmd_share_set () {
    is_shared_dir "$1" && unset_share "$1"

    cmd_share_add "$@"
}

cmd_share_unset () {
    cmd_share_rm "$1" "--all"
}


cmd_team_share() {
    case "${1:-}" in
        ls|list) shift;             cmd_share_list "$@" ;;
        show) shift;                cmd_share_show "$@" ;;
        add) shift;                 cmd_share_add "$@" ;;
        rm|remove) shift;           cmd_share_rm "$@" ;;
        set) shift;                 cmd_share_set "$@" ;;
        unset) shift;               cmd_share_unset "$@" ;;
        reencrypt) shift;           cmd_share_reencrypt "$@" ;;
        help|--help) shift;         cmd_share_usage "$@" ;;
        *)                          cmd_share_usage "$@" ;;
    esac
}

parse_debug_level () {
    case "$1" in
        -d)
            PASSWORD_STORE_TEAM_DEBUG_LEVEL=$((PASSWORD_STORE_TEAM_DEBUG_LEVEL+1))
            ;;
        -dd)
            PASSWORD_STORE_TEAM_DEBUG_LEVEL=$((PASSWORD_STORE_TEAM_DEBUG_LEVEL+2))
            ;;
        -ddd*)
            PASSWORD_STORE_TEAM_DEBUG_LEVEL=$((PASSWORD_STORE_TEAM_DEBUG_LEVEL+3))
            ;;
        --debug)
            PASSWORD_STORE_TEAM_DEBUG_LEVEL=$((PASSWORD_STORE_TEAM_DEBUG_LEVEL+1))
            ;;
    esac
    if [ "$PASSWORD_STORE_TEAM_DEBUG_LEVEL" -gt "3" ] ; then
        PASSWORD_STORE_TEAM_DEBUG_LEVEL="3"
    fi
    __debug $PASSWORD_STORE_TEAM_DEBUG_LEVEL "Set debug level $PASSWORD_STORE_TEAM_DEBUG_LEVEL."
}


cmd_team () {
    case "${1:-}" in
        --no-color)
            set_no_color
            shift;
            cmd_team "$@"
            ;;
        --debug|-d*)
            parse_debug_level "$1"
            shift;
            cmd_team "$@"
            ;;
        -q|--quiet)
            set_quiet
            shift;
            cmd_team "$@"
            ;;
        help|--help|-h)
            shift;
            cmd_team_usage "$@"
            ;;
        init) shift;                cmd_team_init "$@" ;;
        role) shift;                cmd_team_role "$@" ;;
        share) shift;               cmd_team_share "$@" ;;
        version|--version) shift;   cmd_team_version "$@" ;;
        --license) shift;           cmd_team_license "$@" ;;
        *)                          cmd_team_usage "--exit" "$EXIT_USAGE_ERROR" ;;
    esac
}

team_main () {
    cmd_team "$@"
    exit
}


# if not unit testing, run team_main
{ [ -z "${PASS_TEAM_UNIT_TESTS-}" ] && team_main "$@" ; } || true
