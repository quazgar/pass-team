FROM debian:bullseye-slim AS builder

RUN apt-get update && \
    apt-get install -y \
        wget \
        binutils-dev \
        build-essential \
        cmake \
        git \
        libcurl4-openssl-dev \
        libdw-dev \
        libiberty-dev \
        libssl-dev \
        ninja-build \
        python3 \
        zlib1g-dev \
        ;

RUN mkdir -p /tmp && cd /tmp && \
    wget https://github.com/SimonKagstrom/kcov/archive/refs/tags/v40.tar.gz && \
    tar -xvzf v40.tar.gz

RUN mkdir /tmp/kcov-40/build && \
    cd /tmp/kcov-40/build && \
    cmake -G 'Ninja' .. && \
    cmake --build . && \
    cmake --build . --target install

FROM debian:stable
RUN apt-get update
RUN apt-get install -y make
RUN apt-get install -y shunit2
RUN apt-get install -y gnupg
RUN apt-get install -y pass
RUN apt-get install -y shellcheck
RUN apt-get install -y lsb-release
RUN apt-get install -y binutils-dev
RUN apt-get install -y debhelper
RUN apt-get install -y devscripts

RUN apt-get install -y build-essential \
                       procps \
                       jq \
                       curl \
                       file \
                       git \
                       libdw1 \
                       zlib1g \
                       ;

RUN apt-get install -y wget

RUN apt-get clean && rm -rf /var/lib/apt/lists/*

# install kcov
COPY --from=builder /usr/local/bin/kcov* /usr/local/bin/
COPY --from=builder /usr/local/share/doc/kcov /usr/local/share/doc/kcov

# install homebrew
RUN /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
# install pass, so the homebrew installation of pass team is faster
RUN eval "$(/home/linuxbrew/.linuxbrew/bin/brew shellenv)" && brew install pass
