#!/bin/bash

command -v jq
command -v curl
command -v wget

COLLECT_TARGET_VERSION="${COLLECT_TARGET_VERSION:-"$(cat VERSION)"}"
_CURRENT_BRANCH="$(git rev-parse --abbrev-ref HEAD)"
if [ "${_CURRENT_BRANCH}" = "main" ] ; then
    COLLECT_TARGET_REF="${COLLECT_TARGET_BRANCH:-"$COLLECT_TARGET_VERSION"}"
else
    COLLECT_TARGET_REF="${COLLECT_TARGET_BRANCH:-"$(git rev-parse --abbrev-ref HEAD)"}"
fi

echo "Collecting package files for version ${COLLECT_TARGET_VERSION}..."
echo "export VERSION=${COLLECT_TARGET_VERSION}"
echo "export BRANCH=${COLLECT_TARGET_REF}"

_PACKAGES="https://gitlab.com/api/v4/projects/41002690/packages"

function human_friendly_size() {
    numfmt --to=iec --suffix=B "$1"
}

function upper_case() {
    echo "$1" | tr '[:lower:]' '[:upper:]'
}

for package_id in $(curl "${_PACKAGES}" 2>/dev/null | jq -r .[].id) ; do
    echo -n "checking package: ${package_id}: "
    _TMP_PACKAGE_JSON="$(curl "${_PACKAGES}/${package_id}" 2>/dev/null)"
    _TMP_PACKAGE_VERSION="$(echo "${_TMP_PACKAGE_JSON}" | jq -r .version)"
    if [ "$_TMP_PACKAGE_VERSION" = "$COLLECT_TARGET_VERSION" ] ; then
        _TMP_PACKAGE_NAME="$(echo $_TMP_PACKAGE_JSON | jq -r .name)"
        _TMP_PACKAGE_TYPE="$(echo $_TMP_PACKAGE_JSON | jq -r .package_type)"
        echo "[MATCH]"
        echo "  id:      $(echo $_TMP_PACKAGE_JSON | jq -r .id)"
        echo "  name:    $_TMP_PACKAGE_NAME"
        echo "  version: $_TMP_PACKAGE_VERSION"
        readarray -t _TMP_PACKAGE_FILES<<<"$(curl "${_PACKAGES}/${package_id}/package_files" 2>/dev/null | jq -r -c .[])"
        echo "  FILES:"
        for ((i=0; i < ${#_TMP_PACKAGE_FILES[@]}; i++)) ; do
            _TMP_PACKAGE_FILE_JSON="${_TMP_PACKAGE_FILES[$i]}"
            _TMP_PACKAGE_FILE_NAME="$(echo "${_TMP_PACKAGE_FILE_JSON}" | jq -r .file_name)"
            _TMP_PACKAGE_FILE_SIZE="$(echo "${_TMP_PACKAGE_FILE_JSON}" | jq -r .size)"
            _TMP_PACKAGE_FILE_URI="${_PACKAGES}/${_TMP_PACKAGE_TYPE}/${_TMP_PACKAGE_NAME}/${_TMP_PACKAGE_VERSION}/${_TMP_PACKAGE_FILE_NAME}"
            _TMP_PACKAGE_FILE_NAME_ALPHA="$(echo "$_TMP_PACKAGE_FILE_NAME" | tr -dC '[[:alpha:]]')"
            _TMP_KEY_SUFFIX="$(upper_case "${_TMP_PACKAGE_NAME}_${_TMP_PACKAGE_FILE_NAME_ALPHA:(-3)}")"
            echo "     name:  ${_TMP_PACKAGE_FILE_NAME}"
            echo "     size:  $(human_friendly_size "${_TMP_PACKAGE_FILE_SIZE}")"
            echo "      uri:  ${_TMP_PACKAGE_FILE_URI}"
            echo "      key:  ${_TMP_KEY_SUFFIX}"
            echo "export HREF_${_TMP_KEY_SUFFIX}=\"${_TMP_PACKAGE_FILE_URI}\""
            echo "export LABEL_${_TMP_KEY_SUFFIX}=\"${_TMP_PACKAGE_FILE_NAME}\""
            echo "export SIZE_${_TMP_KEY_SUFFIX}=\"$(human_friendly_size "${_TMP_PACKAGE_FILE_SIZE}")\""
        done
    else
        echo "[IGNORING]"
    fi
done

mkdir -p "/tmp/$USER.downloads/"
pushd "/tmp/$USER.downloads/"
_ZIP_URI="https://gitlab.com/pass-team/pass-team/-/archive/${COLLECT_TARGET_REF}/pass-team-${COLLECT_TARGET_REF}.zip"
wget "$_ZIP_URI"
echo "export HREF_SRC_ZIP=\"${_ZIP_URI}\""
echo "export LABEL_SRC_ZIP=\"pass-team-${COLLECT_TARGET_REF}.zip\""
echo "export SIZE_SRC_ZIP=\"$(ls -lah "pass-team-${COLLECT_TARGET_REF}.zip" | awk '{print $5}')\""
rm "pass-team-${COLLECT_TARGET_REF}.zip" || true

_TAR_URI="https://gitlab.com/pass-team/pass-team/-/archive/${COLLECT_TARGET_REF}/pass-team-${COLLECT_TARGET_REF}.tar.gz"
wget "$_TAR_URI"
echo "export HREF_SRC_TAR=\"${_TAR_URI}\""
echo "export LABEL_SRC_TAR=\"pass-team-${COLLECT_TARGET_REF}.tar.gz\""
echo "export SIZE_SRC_TAR=\"$(ls -lah "pass-team-${COLLECT_TARGET_REF}.tar.gz" | awk '{print $5}')\""
rm "pass-team-${COLLECT_TARGET_REF}.tar.gz" || true
popd


