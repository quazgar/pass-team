# Summary

*Please give a short summary of what the issue is. (And thank you sooooo much for taking the time!)*

## Expected Behavior

*What did you expect how the software should behave?*

## Actual Behavior

*What did the software actually do?*

# Steps to Reproduce the Problem

*Please describe, step by step, how others can reproduce the problem. Please try these steps for yourself on a clean system.*

1.
2.
3.

# Specifications

- Pass Team Version: *Which version of this software? Run `pass team --version` to see it.*
* Pass Version: *Which version of pass? Run `pass --version` to see it.*
* Installation Method: *Which installation method has been used? From sources, homebrew, debian package?*
- Platform: *Which operating system, which other relevant software versions?*

# Possible fixes

*Do you have ideas how the issue can be resolved?*
