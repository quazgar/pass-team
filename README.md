<!-- DO NOT CHANGE THIS FILE. It is generated from *.md files in the repository. -->
# Pass Team

`pass team` is an extension for
[pass - the standard unix password manager](https://www.passwordstore.org/).
`pass team` allows team members to share passwords among each other using
Role-based Access Control.

```bash
# Create a new role in your team
pass team role create   dev
```

```bash
# Assign your team members
pass team role assign   dev   ali@example.org   mia@example.org
```

```bash
# Share passwords with them
pass team share set     dir/in/my/password-store   dev
```

See [Examples](./docs/examples/) for extensive usage examples.

## Current Features

* Initialize a team with team managers
  `pass team init ([--trusted] <gpg-id>)...`
  * normal team managers can manage roles and shares.
  * Optionally, trusted team managers additionally have read-access to all
    passwords.
* Manage roles (list/add/remove/assign/unassign).
  `pass team role ...`
* Manage shares (list/add/remove)
  `pass team share`
* Automatically reencrypt passwords after roles or shares have been changed.

For more information about the available commands see the
[Man Page](./man/pass-team.md).


## Vision, Goals, Roadmap and Releases

`pass team` is work-in-progress and has not been released yet. The upcoming
version is **0.1**. It covers basic Role-based Access Control features.

**Warning:** `pass team` has not been tested with a git setup yet. It is
planned for the future release 0.2 to test this and add convenient .gitignore
and config settings for sharing pass team data over git repositories.

See the [Roadmap](./ROADMAP.md) and the
[Vision and Goals](./VISION_AND_GOALS.md) for more information.


## Concepts

`pass team` implements Role-based Access Control for passwords in the password
store.

1. Team members are identified by their gpg public keys and have roles.
2. Directories in the password store can be shared with team members by
   assigning roles to the shares.
3. Passwords in the shared directories are being encrypted for all users who
   have the correct roles.
4. "Pass team managers" are privileged team members who can manage the roles of
   the team members and the shared directories.

See a more technical description in [concepts.md](./docs/concepts.md).

# Installation From Packages

## Debian Package

**For Debian and Debian-based Linux distros like Ubuntu, Kali, or Mint. You need
privileged access. This also installs pass and all other dependencies.**

1. Get the debian package [here](https://gitlab.com/pass-team/pass-team/-/releases/).
2. In your download directory, run

    ```bash
    sudo apt-get install ./pass-extension-team_0.1.0-rc.11-1_all.deb
    ```


## Homebrew Formula

**For any OS which supports [Homebrew](https://docs.brew.sh/Installation),
especially MacOS, but also Windows Subsystem for Linux (WSL). This also
installs pass and all other dependencies.**

1. Get the homebrew formula
   [here](https://gitlab.com/pass-team/pass-team/-/releases/).

2. In your download directory, run

    ```bash
    brew install ./pass-team.rb
    ```


## Manual Installation on \*nix

**For any system with GNU Make.**

### System-wide

**You need privileged access for this. Pass needs to be installed already.**

1. Get the repository tarball
   [here](https://gitlab.com/pass-team/pass-team/-/releases/) or clone the
   repository. Unpack the tarball somewhere.

2.  In the unpacked repository, run

    ```bash
    sudo make install
    ```

    This installs the pass team extension itself, as well as bash-completion and the man pages.

### Locally

**Pass needs be installed already.**

1. Get the repository tarball
   [here](https://gitlab.com/pass-team/pass-team/-/releases/) or clone the
   repository. Unpack the tarball somewhere.

2.  In the unpacked repository, run

    ```bash
    make PASSWORD_STORE_EXTENSIONS_INSTALL_DIR=<directory> install
    ```

    The `PASSWORD_STORE_EXTENSIONS_INSTALL_DIR` is the password store's
    extensions directory where pass team is to be installed (as a user
    extension). Usually this is `~/.password-store/.extensions` or whatever
    `$PASSWORD_STORE_EXTENSIONS_DIR` says.

    You have to install pass team in every password store you want to use it,
    if you cannot install the extension system-wide.


## See Also

* [Installation from the sources](./docs/installation_from_sources.md)

# Installation From the Sources

## Globally, with GNU Make

* Run `sudo make install`

## Globally, by Hand

* Copy `src/team.bash` to
  `/usr/lib/password-store/extensions`.
* Source `src/team.completion.bash` somewhere.

## As User Extension

* Copy `src/team.bash` to `~/.password-store/.extensions` or wherever you
  initialized your password store. (Check if the `PASSWORD_STORE_DIR` variable
  is set if you can't find it.)
* Add `export PASSWORD_STORE_ENABLE_EXTENSIONS=true` to your `~/.bashrc` or be
  sure to have this variable being set otherwise in order to activate user
  extensions. If you have already installed other user extensions this step is
  probably not necessary.
* Source `src/team.completion.bash` somewhere.

# Uninstall Pass Team

* Recommended first step: "Unshare" all shared directories via `pass team share
  unset ...`. You need the private keys for all the passwords of course.
  Otherwise the passwords cannot be converted into normal, private passwords
  and you can as well delete the passwords of that share.
* Remove the `~/.passwords-store/.team` directory.
* Remove the `team.bash` executable from your (global or user-specific)
  extensions directory.
* If you installed the extension as a user extension, consider unsetting the
  `PASSWORD_STORE_ENABLE_EXTENSIONS` variable (unless you need it for any other
  user extensions).


# Compatibility

This `pass team` has been tested with:
* Debian oldoldstable (stretch), `pass` v1.7.1 (from backports). Auto-completion has not been tested here.
* Debian oldstabel (buster), `pass` v1.7.3
* Debian stable (bullseye), `pass` v1.7.3,
* Debian unstable (sid), `pass` v1.7.4
* Debian testing (bookworm), `pass` v1.7.4
* Gentoo, `pass` v1.7.4
* Does not work: Debian oldoldstable (stretch) with `pass` v1.6.5 and most likely any older release.

# Examples

Usage examples for `pass team` from a fresh setup to a populated password store with shared and personal passwords.


## Fresh Setup

[fresh_setup.sh](./docs/examples/fresh_setup.sh) - Example for a fresh setup of `pass team`.


## Roles Management

[roles_management.sh](./docs/examples/roles_management.sh) - Examples for listing, adding, viewing, assigning, unassigning and removing roles. This example is build upon the [Fresh Setup](./docs/examples/fresh_setup.sh) example.


## Shared Password Directories

[shared_password_directories.sh](./docs/examples/shared_password_directories.sh) - Example for sharing and unsharing directories, assigning roles to the shared directories and inspecting and listing shared directories. This example is build upon the [Roles Management](./docs/examples/roles_management.sh) example.


## Read and Write Passwords

[read_and_write_passwords.sh](./docs/examples/read_and_write_passwords.sh) - Example for reading and writing passwords in the shared directories. This example is build upon the [Shared Password Directories](./docs/examples/shared_password_directories.sh) example.


## Reencryption After Changes

This example shows how passwords are automatically being reencrypted after changes to the roles or the shares. This example is build upon the [Read and Write Passwords](./docs/examples/read_and_write_passwords.sh) example.

# Tests

## Requirements

* \>=bash-4.4
* shunit2
* shellcheck
* make

## Optionally

* kcov 40 for test coverage reports

## All Tests

Run `make test` to run all tests including linting, unit tests, integration tests and tests of the bash completion.

## Linting

We use `shellcheck` for linting. Run `make linting` to check all source files.

## Unit Tests

The unit test suite is in `test/unit-tests.bash`. These tests do not require
a prior installation of pass or gpg.

Run unit tests via `make unit-tests`.

## Integration Tests

The integration test suite is in `test/integration-tests.bash`. These tests
require a prior installation of `pass` and `gpg` and test the integration of
the three programs with some use cases.

Run the integration tests via `make integration-tests`

## Test the Bash-Completion

The test suite for the bash-completion is in `test/completion-tests.bash`.
These tests require a prior installation of `pass` including the
bash-completion of `pass`.

Run bash-completion tests via `make bash-completion-tests`

## Test Coverage Report

If the `COVERAGE_OUTDIR` environment variable is set to an **absolute**
directory path, exported, and `kcov` is installed the tests will generate test
coverage reports in the given `COVERAGE_OUTDIR` directory.

## Manually Goofing Around

1. Run `make playground`. This
   * Creates a new gnupg directory and keyring at `$PLAYGROUND_DIR` which
     defaults to `tmp/playground`.
   * Creates several pgp keys (`passt-manager{1,2}`, `passt-user{1-7}`).
   * Initializes a password store at `${PLAYGROUND_DIR}/.password-store/`.
   * Installs pass-teams in the
     `${PLAYGROUND_DIR}/.password-store/.extensions/` directory.
2. Run `source ${PLAYGROUND_DIR}/source.me`. Now, you can try out the pass team
   commands, starting with `pass team ...`

# Contributing

Thank you very much to all contributers—[past, present](./docs/CREDITS.md), and
prospective ones.

## Code of Conduct

By participating, you are expected to uphold our
[Code of Conduct](./docs/CODE_OF_CONDUCT.md).

## How to Contribute

* You found a bug, have a question, or want to request a feature? Please create
  a [Gitlab issue](https://gitlab.com/pass-team/pass-team/-/issues/new).
* You want to contribute code or write documentation? I'd really appreciate
  that. Please contact me at <t.fitschen@indiscale.com>, send me git-diffs, fork
  the project and create merge requests, or put your ideas into an issue.

## Merge Requests and Code Review

Please, prepare your MR for a review: Be sure to write a summary section and a
focus section. The summary should summarize what your MR is about. The focus
should point the reviewer to the most important code changes.

Also, create gitlab comments (aka "annotations") for the reviewer. They should
guide the reviewer through the changes, explain your changes and also point out
open questions.

For further good practices have a look at
[the caosdb review guidelines](https://gitlab.com/caosdb/caosdb/-/blob/dev/REVIEW_GUIDELINES.md).

# Credits

* [pass - the standard unix password manager](https://www.passwordstore.org/)
   was written by Jason A. Donenfeld of [zx2c4.com](https://www.zx2c4.com/)
   with contributions from a loving community.

   The `pass team` extension is also inspired by Jason:
   > Multiple GPG keys can be specified, for using pass in a team setting, and
   different folders can have different GPG keys, by using -p.

   This quote from the [pass website](https://www.passwordstore.org/) got this
   project going.
* [IndiScale](https://www.indiscale.com) supported this project with their
  time, feedback and other resources.

# License (GPLv3 or later)

`pass team` is an extension for
[pass - the standard unix password manager](https://www.passwordstore.org/).
`pass team` allows team members to share passwords among each other using
Role-based Access Control.

Copyright (C) 2021-2022 Timm C. Fitschen <t.fitschen@indiscale.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
[GNU General Public License](./docs/GPLv3.md) for more details.

