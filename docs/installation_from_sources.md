# Installation From the Sources

## Globally, with GNU Make

* Run `sudo make install`

## Globally, by Hand

* Copy `src/team.bash` to
  `/usr/lib/password-store/extensions`.
* Source `src/team.completion.bash` somewhere.

## As User Extension

* Copy `src/team.bash` to `~/.password-store/.extensions` or wherever you
  initialized your password store. (Check if the `PASSWORD_STORE_DIR` variable
  is set if you can't find it.)
* Add `export PASSWORD_STORE_ENABLE_EXTENSIONS=true` to your `~/.bashrc` or be
  sure to have this variable being set otherwise in order to activate user
  extensions. If you have already installed other user extensions this step is
  probably not necessary.
* Source `src/team.completion.bash` somewhere.

# Uninstall Pass Team

* Recommended first step: "Unshare" all shared directories via `pass team share
  unset ...`. You need the private keys for all the passwords of course.
  Otherwise the passwords cannot be converted into normal, private passwords
  and you can as well delete the passwords of that share.
* Remove the `~/.passwords-store/.team` directory.
* Remove the `team.bash` executable from your (global or user-specific)
  extensions directory.
* If you installed the extension as a user extension, consider unsetting the
  `PASSWORD_STORE_ENABLE_EXTENSIONS` variable (unless you need it for any other
  user extensions).

