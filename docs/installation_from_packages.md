# Installation From Packages

## Debian Package

**For Debian and Debian-based Linux distros like Ubuntu, Kali, or Mint. You need
privileged access. This also installs pass and all other dependencies.**

1. Get the debian package [here](https://gitlab.com/pass-team/pass-team/-/releases/).
2. In your download directory, run

    ```bash
    sudo apt-get install ./pass-extension-team_0.1.0-rc.11-1_all.deb
    ```


## Homebrew Formula

**For any OS which supports [Homebrew](https://docs.brew.sh/Installation),
especially MacOS, but also Windows Subsystem for Linux (WSL). This also
installs pass and all other dependencies.**

1. Get the homebrew formula
   [here](https://gitlab.com/pass-team/pass-team/-/releases/).

2. In your download directory, run

    ```bash
    brew install ./pass-team.rb
    ```


## Manual Installation on \*nix

**For any system with GNU Make.**

### System-wide

**You need privileged access for this. Pass needs to be installed already.**

1. Get the repository tarball
   [here](https://gitlab.com/pass-team/pass-team/-/releases/) or clone the
   repository. Unpack the tarball somewhere.

2.  In the unpacked repository, run

    ```bash
    sudo make install
    ```

    This installs the pass team extension itself, as well as bash-completion and the man pages.

### Locally

**Pass needs be installed already.**

1. Get the repository tarball
   [here](https://gitlab.com/pass-team/pass-team/-/releases/) or clone the
   repository. Unpack the tarball somewhere.

2.  In the unpacked repository, run

    ```bash
    make PASSWORD_STORE_EXTENSIONS_INSTALL_DIR=<directory> install
    ```

    The `PASSWORD_STORE_EXTENSIONS_INSTALL_DIR` is the password store's
    extensions directory where pass team is to be installed (as a user
    extension). Usually this is `~/.password-store/.extensions` or whatever
    `$PASSWORD_STORE_EXTENSIONS_DIR` says.

    You have to install pass team in every password store you want to use it,
    if you cannot install the extension system-wide.


## See Also

* [Installation from the sources](./installation_from_sources.md)
