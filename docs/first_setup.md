# First Setup

After you have installed `pass team` you need to initialize the extension and
configure your team.

## Initialize Pass Team as an Administrator

1. You need at least one gpg key pair. It is possible to use setups with more
   keys, but these are not covered here. Let your key id be "passt-manager1".

2. You need the public keys of all team members in your GPG key store. We call
   them `passt-user[2-7]`.

3. Do `pass team init passt-user1` - Now pass team is set up.

4. Add roles, e.g.: `pass team role create boss`. Add more roles as appropriate,
   e.g. `cr`, `hr`, `dev`, `person1`, `person2`, `person3`, `customer1`,
   `customer2`, `customer3`.

5. Assign all team members to the appropriate roles, e.g.
   * `pass team role assign hr passt-user1 passt-user2`
   * `pass team role assign cr passt-user1 passt-user3 passt-user4`
   * `pass team role assign boss passt-user5`
   etc.

6. Share one or more directories in the password store with a role:
    * `pass team share add boss-dir boss`
    * `pass team share add humanresources-dir humanresources boss`
    * `pass team share add customerrelations-dir customerrelations boss
      marketing`
    * `pass team share add development-dir development boss`
    * `pass team share add marketing-dir marketing boss`

Now you are set up. If anyone stores a new password under these directories,
they are automatically readable for anyone with the given role.

See the [examples](./examples) for more usage examples.

## Initialize as a Normal Team Member

We assume that you want to participate in an existing team and you are not
responsible for managing the team's roles or share. In this case there is no
special setup needed. You don't even have to install the `pass team` extension.

You just syncronize your local password store with the teams password store
(e.g. via `pass git`) and use `pass` as you would normally do.

You only need the team members' public keys present in your gpg key ring if you
want to insert or edit shared passwords.
