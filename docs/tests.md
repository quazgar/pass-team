# Tests

## Requirements

* \>=bash-4.4
* shunit2
* shellcheck
* make

## Optionally

* kcov 40 for test coverage reports

## All Tests

Run `make test` to run all tests including linting, unit tests, integration tests and tests of the bash completion.

## Linting

We use `shellcheck` for linting. Run `make linting` to check all source files.

## Unit Tests

The unit test suite is in `test/unit-tests.bash`. These tests do not require
a prior installation of pass or gpg.

Run unit tests via `make unit-tests`.

## Integration Tests

The integration test suite is in `test/integration-tests.bash`. These tests
require a prior installation of `pass` and `gpg` and test the integration of
the three programs with some use cases.

Run the integration tests via `make integration-tests`

## Test the Bash-Completion

The test suite for the bash-completion is in `test/completion-tests.bash`.
These tests require a prior installation of `pass` including the
bash-completion of `pass`.

Run bash-completion tests via `make bash-completion-tests`

## Test Coverage Report

If the `COVERAGE_OUTDIR` environment variable is set to an **absolute**
directory path, exported, and `kcov` is installed the tests will generate test
coverage reports in the given `COVERAGE_OUTDIR` directory.

## Manually Goofing Around

1. Run `make playground`. This
   * Creates a new gnupg directory and keyring at `$PLAYGROUND_DIR` which
     defaults to `tmp/playground`.
   * Creates several pgp keys (`passt-manager{1,2}`, `passt-user{1-7}`).
   * Initializes a password store at `${PLAYGROUND_DIR}/.password-store/`.
   * Installs pass-teams in the
     `${PLAYGROUND_DIR}/.password-store/.extensions/` directory.
2. Run `source ${PLAYGROUND_DIR}/source.me`. Now, you can try out the pass team
   commands, starting with `pass team ...`
