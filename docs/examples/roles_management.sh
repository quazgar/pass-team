#!/bin/bash

# # Roles Management

# We assume that pass and pass-team have been initialized as shown in
# ./fresh_setup.sh


# Create some roles:
pass team role create developer manager


# Assign some subjects to the roles:
pass team role assign developer passt-user1

pass team role assign manager passt-user2


# Show particular roles:
pass team role show developer


# Remove a role:
pass team role create dummy-role

pass team role remove dummy-role


# List all roles:
pass team role list


# Unassign a subject:
pass team role assign developer passt-user2

pass team role unassign developer passt-user2


# Error handling:
pass team role assign unknown-role passt-user1

pass team role assign developer unknown-subject
