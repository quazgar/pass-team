#!/bin/bash

# # Read and Write Passwords

# We assume shared directories have been created as shown in
# ./shared_password_directories.sh


# Insert some shared passwords:
pass generate my_team/management/boss_password > /dev/null

pass generate my_team/development/dev_password > /dev/null


# We *can* read this one because we have the private keys for passt-user1.
pass show my_team/development/dev_password


# We *cannot* read this one because we do not have the private keys.
pass show my_team/management/boss_password


# Pass handles your other passwords as you would it expect it. You can add some
# non-shared passwords as well.
pass generate private/path/personal_password > /dev/null

pass show private/path/personal_password


# List all passwords
pass list
