#!/bin/bash

## Reencryption

# We assume that shared passwords been generated as shown in
# ./read_and_write_passwords.sh

# After changes of the roles or of the shared directories it is necessary to
# reencrypt the shared passwords - otherwise, newly assigned subjects or roles
# would not be able to read the passwords or removed roles would still be able.
# This happens automatically.

### Changing Shares

# Share a directory with the developer role
pass team share add my_team/some_dir/ developer

# Insert a password
pass generate my_team/some_dir/some_password > /dev/null

# Now add the manager role to the shared directory
pass team share add my_team/some_dir/ manager

# And remove the developer role
pass team share remove my_team/some_dir/ developer

### No Secret Key

# However, if the agent does not have the read-permission for the shared
# directory, the reencryption fails. We cannot just add the developer role
# again:
pass team share set my_team/some_dir/ developer

# The password store and the team is not in sync now. The team extension says the directory is shared with the developer role
pass team share show my_team/some_dir

# But gpg says it is encrypted for the `passt-user2` who doesn't have the developer role but the manager role.
gpg -d $PASSWORD_STORE_DIR/my_team/some_dir/some_password.gpg

# This is easily fixed by reverting the operation
pass team share set my_team/some_dir/ manager

### Changing Roles

# Everything that shown above also applies for changing the roles.

# It is possible to assign a new `passt-user3` to the developer role. All shared passwords are reencrypted for `passt-user3`.
pass team role assign developer passt-user3

# However, it is not possible to add yourself to the manager role and reencrypt
# the manager's passwords.
pass team role assign manager passt-user1

### How to Deal With 'No Secret Key'

# It is necessary to have someone perform the changes who can read the
# passwords. So adding someone to the manager role should be done by team
# members who have the manager role themselves. Sharing a directory with
# another role should also done by those who can already read it.

# As we have seen above, both works well.

# When a team manager changed the assignments of a role or the shared directory
# and they could not reencrypt the shared passwords, they can also inform
# someone who *can* read the shared passwords to run
#
# ```bash
# pass team share reencrypt my_team/some_dir
# ```
#
# on their behalf. This will reencrypt the shared passwords in
# `my_team/some_dir` accordingly.

# As another option, it is possible to initialize `pass team` with a
# `--trusted` manager. A `--trusted` manager can read *all* passwords and thus
# they can also reencrypt passwords whenever this is necessary. Read more about
# the `--trusted` flag on the [Man Page](../../man/pass-team.md)
