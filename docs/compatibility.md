# Compatibility

This `pass team` has been tested with:
* Debian oldoldstable (stretch), `pass` v1.7.1 (from backports). Auto-completion has not been tested here.
* Debian oldstabel (buster), `pass` v1.7.3
* Debian stable (bullseye), `pass` v1.7.3,
* Debian unstable (sid), `pass` v1.7.4
* Debian testing (bookworm), `pass` v1.7.4
* Gentoo, `pass` v1.7.4
* Does not work: Debian oldoldstable (stretch) with `pass` v1.6.5 and most likely any older release.
