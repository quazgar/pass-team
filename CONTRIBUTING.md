# Contributing

Thank you very much to all contributers—[past, present](./CREDITS.md), and
prospective ones.

## Code of Conduct

By participating, you are expected to uphold our
[Code of Conduct](./CODE_OF_CONDUCT.md).

## How to Contribute

* You found a bug, have a question, or want to request a feature? Please create
  a [Gitlab issue](https://gitlab.com/pass-team/pass-team/-/issues/new).
* You want to contribute code or write documentation? I'd really appreciate
  that. Please contact me at <t.fitschen@indiscale.com>, send me git-diffs, fork
  the project and create merge requests, or put your ideas into an issue.

## Merge Requests and Code Review

Please, prepare your MR for a review: Be sure to write a summary section and a
focus section. The summary should summarize what your MR is about. The focus
should point the reviewer to the most important code changes.

Also, create gitlab comments (aka "annotations") for the reviewer. They should
guide the reviewer through the changes, explain your changes and also point out
open questions.

For further good practices have a look at
[the caosdb review guidelines](https://gitlab.com/caosdb/caosdb/-/blob/dev/REVIEW_GUIDELINES.md).
