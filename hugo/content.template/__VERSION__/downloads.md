---
title: Downloads
layout: downloads
summary: "Download links for the Debian and Homebrew packages as well as the source code."
---

Read the [installation guide]({{< ref "./docs/installation" >}}).
