---
title: Shared Password Directories
summary: "Example for sharing and unsharing directories, assigning roles to the shared directories and inspecting and listing shared directories. This example is build upon the [Roles Management](./roles_management/) example."
weight: 300
---
