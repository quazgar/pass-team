---
title: "Reencryption After Changes"
summary: "This example shows how passwords are automatically being reencrypted after changes to the roles or the shares."
weight: 500
---
