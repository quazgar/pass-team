---
title: Read and Write Passwords
summary: 'Example for reading and writing passwords in the shared directories. This example is build upon the [Shared Password Directories](./shared_password_directories/) example.'
weight: 400
---
