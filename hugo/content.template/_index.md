---
title: "Pass Team"
sitemap:
  priority: 1.0
---
Share your passwords with your team.  
`pass team` is an extension for [pass - the standard unix password manager](https://www.passwordstore.org/).
