---
title: Installation
summary: How to install `pass team` on your system.
weight: 200
---

## Debian Package

*For Debian and Debian-based Linux distros like Ubuntu, Kali, or Mint. You need
privileged access. This also installs pass and all other dependencies.*

1. Get the debian package [here](https://gitlab.com/pass-team/pass-team/-/releases/).
2. In your download directory, run

    ```bash
    sudo dpkg install pass-extension-team-*.deb
    ```


## Homebrew Formula

*For any OS which supports [Homebrew](https://docs.brew.sh/Installation),
especially MacOS, but also Windows Subsystem for Linux (WSL). This also
installs pass and all other dependencies.*

1. Get the homebrew formula (pass-extension-team.rb)
   [here](https://gitlab.com/pass-team/pass-team/-/releases).

2. In your download directory, run

    ```bash
    brew install pass-extension-team.rb
    ```

## Manual Installation on \*nix

*For any system with GNU Make.*

### System-wide

*You need privileged access for this. Pass needs to be installed already.*

1. Get the repository tarball
   [here](https://gitlab.com/pass-team/pass-team/-/releases) or clone the
   repository. Unpack the tarball somewhere.

2. In the unpacked repository, run

    ```bash
    sudo make install
    ```

### Locally

*Pass needs be installed already.*

1. Get the repository tarball
   [here](https://gitlab.com/pass-team/pass-team/-/releases) or clone the
   repository. Unpack the tarball somewhere.

2. In the unpacked repository, run

    ```bash
    make PASSWORD_STORE_DIRECTORY=<directory> install
    ```

    The `PASSWORD_STORE_DIRECTORY` is the password store where pass team is to
    be installed (as a user extension). You have to install pass team in every
    password store you want to use it, if you cannot install the extension
    system-wide.

## See Also

* [Installation from the sources](https://gitlab.com/pass-team/pass-team/-/blob/0.1.0-rc.2/docs/installation_from_sources.md)
