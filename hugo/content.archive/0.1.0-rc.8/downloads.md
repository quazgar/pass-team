---
title: Downloads
layout: downloads
summary: "Download links for the Debian and Homebrew packages as well as the source code."
---

Read the installation guide [here]({{< ref "./docs/installation" >}}).
