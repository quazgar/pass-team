---
title: Introduction
weight: 100
summary: "Introduction to the features of `pass team`, basic concepts and ideas."
sitemap:
  priority: 1.0
---

`pass team` is an extension for
[pass - the standard unix password manager](https://www.passwordstore.org/).
`pass team` allows team members to share passwords among each other using
Role-based Access Control.

```bash
# Create a new role in your team
pass team role create   dev
```

```bash
# Assign your team members
pass team role assign   dev   ali@example.org   mia@example.org
```

```bash
# Share passwords with them
pass team share set     dir/in/my/password-store   dev
```

See [Examples]({{< ref "../examples/" >}}) for extensive usage examples.

## Current Features

* Initialize a team with team managers
  `pass team init ([--trusted] <gpg-id>)...`
  * normal team managers can manage roles and shares.
  * Optionally, trusted team managers additionally have read-access to all
    passwords.
* Manage roles (list/add/remove/assign/unassign).
  `pass team role ...`
* Manage shares (list/add/remove)
  `pass team share`
* Automatically reencrypt passwords after roles or shares have been changed.

For more information about the available commands see the
[Man Page]({{< ref "./man-pass-team.md" >}}).


## Vision, Goals, Roadmap and Releases

`pass team` is work-in-progress and has not been released yet. The upcoming
version is **0.1**. It covers basic Role-based Access Control features.

**Warning:** `pass team` has not been tested with a git setup yet. It is
planned for the future release 0.2 to test this and add convenient .gitignore
and config settings for sharing pass team data over git repositories.

See the [Roadmap]({{< ref "/ROADMAP.md" >}}) and the
[Vision and Goals]({{< ref "/VISION_AND_GOALS.md" >}}) for more information.


## Concepts

`pass team` implements Role-based Access Control for passwords in the password
store.

1. Team members are identified by their gpg public keys and have roles.
2. Directories in the password store can be shared with team members by
   assigning roles to the shares.
3. Passwords in the shared directories are being encrypted for all users who
   have the correct roles.
4. "Pass team managers" are privileged team members who can manage the roles of
   the team members and the shared directories.

See a more technical description in [concepts.md]({{< ref "./concepts.md" >}}).
