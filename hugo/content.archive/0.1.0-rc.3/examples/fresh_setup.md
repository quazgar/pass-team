---
title: Fresh Setup
summary: "Example for a fresh setup of `pass team`."
weight: 100
---



We assume that the gpg key store is ready and contains private keys for:
* passt-manager1
* passt-user1

as well as a public key for:
* passt-user2


Initialize a fresh password store:


```bash
pass init passt-user1
```

```bash_out
Password store initialized for passt-user1
```

Initialize a fresh team:


```bash
pass team init passt-manager1
```

```bash_out
mkdir: created directory '/home/tf/.password-store/.team'
Password store initialized for 5C28F10DC2DA031B8CDE8C0B762466487994670C (.team)
```


*Generated from [fresh_setup.sh](https://gitlab.com/pass-team/pass-team/-/blob/0.1.0-rc.3/docs/examples/fresh_setup.sh)*
