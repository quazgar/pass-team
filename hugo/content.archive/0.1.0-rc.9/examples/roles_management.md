---
title: Roles Management
summary: "Examples for listing, adding, viewing, assigning, unassigning and removing roles. This example is build upon the [Fresh Setup](./fresh_setup/) example."
weight: 200
---



We assume that pass and pass-team have been initialized as shown in
[fresh_setup.md]({{< ref "./fresh_setup.md" >}})


Create some roles:


```bash
pass team role create developer manager
```

```bash_out
Created role 'developer'.
Created role 'manager'.
```

Assign some subjects to the roles:


```bash
pass team role assign developer passt-user1
```

```bash_out
Assigned role 'developer' to subject 'passt-user1'.
```

```bash
pass team role assign manager passt-user2
```

```bash_out
Assigned role 'manager' to subject 'passt-user2'.
```

Show particular roles:


```bash
pass team role show developer
```

```bash_out
Role: developer
└── GPG Keys:
    └── 4209A130F10F062A ── passt-user1
```

Remove a role:


```bash
pass team role create dummy-role
```

```bash_out
Created role 'dummy-role'.
```

```bash
pass team role remove dummy-role
```

```bash_out
Removed role 'dummy-role'.
```

List all roles:


```bash
pass team role list
```

```bash_out
Roles
├── developer
└── manager
```

Unassign a subject:


```bash
pass team role assign developer passt-user2
```

```bash_out
Assigned role 'developer' to subject 'passt-user2'.
```

```bash
pass team role unassign developer passt-user2
```

```bash_out
Unassigned role 'developer' from subject 'passt-user2'.
```

Error handling:


```bash
pass team role assign unknown-role passt-user1
```

```bash_err
Error: The role 'unknown-role' does not exist.
       Please add it to your pass team.

(exit 86)
```

```bash
pass team role assign developer unknown-subject
```

```bash_err
Error: The public key of 'unknown-subject' is unknown.        
       Please add it to your gpg keyring.

(exit 84)
```


*Generated from [roles_management.sh](https://gitlab.com/pass-team/pass-team/-/blob/0.1.0-rc.9/docs/examples/roles_management.sh)*
