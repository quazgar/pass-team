---
title: man pass-team(1)
date: 2022-12-06
styling: "man-page"
summary: "The unix man page for `pass team` explains the most important commands."
---
# NAME

*pass team* -- An extension for [pass(1)](https://git.zx2c4.com/password-store/about/) which allows team members to share
passwords among each other using Role-based Access Control.

# SYNOPSIS

*`pass team`* ` --version|--license|--help`

*`pass team`* ` [OPTIONS] COMMANDS...`

*`pass team init`* ` ([--trusted] <gpg-key>)...`

*`pass team role`* ` list`

*`pass team role`* ` (create|show|remove) <role>...`

*`pass team role`* ` (assign|unassign) <role> <gpg key>...`

*`pass team share`* ` list`

*`pass team share`* ` (show|unset|reencrypt) <shared dir>`

*`pass team share`* ` (add|set|remove) <share dir> <role>...`

# DESCRIPTION

Pass team is an extension for pass - the standard unix password manager
(https://www.passwordstore.org/\).

Pass team allows team members to use pass as the password store for both
private and shared passwords and distribute shared passwords among each other
using Role-based Access Control.

# OPTIONS

\--quiet|-q
: Display less output which is easier to process for non-human readers.
Alternatively, set the the **`PASSWORD_STORE_TEAM_QUIET`** environment variable
to any value.

\--help|-h
:   Show help and exit. When combined with *COMMANDS* a more detailed help is
printed.

\--debug|-d
:   Print debug messages. Can be repeated several times for more verbose debug
messages. Alternatively, use the environment variable
**`PASSWORD_STORE_TEAM_DEBUG_LEVEL=[0-3]`** for setting a debug level.

\--licence
:   Show licence and exit.

\--version
:   Show version information for this extension and exit.

\--no-color
:   Disable colored output. Alternatively, set the **`NO_COLOR`** or
**`PASSWORD_STORE_TEAM_NO_COLOR`** environment variable to any value.

# COMMANDS

## init (\[\--trusted\] \<gpg-key\>)\...

Intialize the pass team extensions and use the given gpg-key as pass team
managers. The pass team managers are responsible for managing the roles and
shared directories. Managers who are 'trusted' will also be added to every
share (and thus will be able to read every password). This is handy when
passwords need to be reencrypted often because of changing roles or shares.

The **`--trusted`** flag always applies to the next directly following gpg-key.

## role

Commands for adding, removing, assigning role and more.

list
:   List all known roles. When combined with the **`-q`** OPTION all roles will
be printed space-separated in one line.

show \<role\>
:   Show the role and the assigned gpg-ids.

create \<role\>
:   Create a new role in the team.

remove \<role\>
:   Remove the role from the team

assign \<role\> \<gpg-key\>...
:   Assign the given role to all gpg-ids.

unassign \<role\> \<gpg-key\>...
:   Unassign the given role from all gpg-ids.

## share

list
:   List all shared directories. When combined with the -q OPTION all all
shared directories space-separated in one line.

show \<directory\>
:   Show all roles which have access to the given directory.

add \<directory\> \<role\>...
:   Add the roles to the shared directory. Same as **`pass team share set`** if
    the directory has not been shared previously.

set \<directory\> \<role\>...
:   Add the directory to the list of shared directories and share it with all
    of the given roles and remove any other roles from this share.

reencrypt (\--all|\<directory\>...)
:   Reencrypt the given directories. This command is useful after an
    administrator changed the role assignments or the shared directory and yet
    could not reencrypt the shared passwords accordingly because they were
    lacking the read permission for the shared directory.

remove \<directory\> (\--all|\<role\>...)
:   Remove the roles from the shared directory. **`--all`** removes the shared
    directory completely. If the directory is not being shared with any role
    anymore, the directory is removed from the list of shared directories and
    all passwords are reencrypted for the local password store.

unset \<directory\>

:   Alias for *`pass team share remove --all`*.

# ENVIRONMENT

PASSWORD_STORE_TEAM_NO_COLOR|NO_COLOR
: Set to any value to disable colored output. Alternatively, use the
**`--no-color`** flag.

PASSWORD_STORE_TEAM_QUIET
: Set to any value to display less output which is easier to process for
non-human readers. Alternatively, use the **`--quiet`** flag.

PASSWORD_STORE_TEAM_DEBUG_LEVEL=[0-3]
: Set debug level. A level of '0' does not output any debug messages. Higher
levels output more verbose debug messages. Alternatively, use the **`--debug`**
flags.

# SEE ALSO

[pass(1)](https://git.zx2c4.com/password-store/about/), [git(1)](https://manpages.org/git), [gpg(1)](https://manpages.org/gpg)

# COPYING

Copyright (C) 2021-2022 Timm C. Fitschen <t.fitschen@indiscale.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
