---
title: Read and Write Passwords
summary: 'Example for reading and writing passwords in the shared directories. This example is build upon the [Shared Password Directories](./shared_password_directories/) example.'
weight: 400
---



We assume shared directories have been created as shown in
[shared_password_directories.md]({{< ref "./shared_password_directories.md" >}})


Insert some shared passwords:


```bash
pass generate my_team/management/boss_password > /dev/null
```


```bash
pass generate my_team/development/dev_password > /dev/null
```


We *can* read this one because we have the private keys for passt-user1.


```bash
pass show my_team/development/dev_password
```

```bash_out
3BbM1q3GutvrGUo0jGLLjGScV
```

We *cannot* read this one because we do not have the private keys.


```bash
pass show my_team/management/boss_password
```

```bash_err
gpg: decryption failed: No secret key

(exit 2)
```

Pass handles your other passwords as you would it expect it. You can add some
non-shared passwords as well.


```bash
pass generate private/path/personal_password > /dev/null
```


```bash
pass show private/path/personal_password
```

```bash_out
phRD4v2BsrLqMzHdic6C7tiXj
```

List all passwords


```bash
pass list
```

```bash_out
Password Store
├── my_team
│   ├── development
│   │   └── dev_password
│   └── management
│       └── boss_password
└── private
    └── path
        └── personal_password
```


*Generated from [read_and_write_passwords.sh](https://gitlab.com/pass-team/pass-team/-/blob/0.1.0-rc.10/docs/examples/read_and_write_passwords.sh)*
