---
title: 'Release Candidate 0.1.0-rc.7'
publishdate: 2022-11-15
---

The version 0.1.0-rc.7 is a release candidate. New features will not be implemented in the up-coming version 0.1.0. Bug fixes and updates to the documentation may still be included into the target version.
