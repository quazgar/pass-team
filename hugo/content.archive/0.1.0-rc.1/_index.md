---
title: "Version 0.1.0-rc.1"
menu: version
publishdate: 2022-05-17
---

The version `0.1.0-rc.1` is a release candidate. New features will not be implemented in the up-coming version `0.1.0`. Bug fixes and updates to the documentation may still be included into the target version.
