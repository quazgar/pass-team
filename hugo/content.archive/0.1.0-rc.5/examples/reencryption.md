---
title: "Reencryption After Changes"
summary: "This example shows how passwords are automatically being reencrypted after changes to the roles or the shares."
weight: 500
---



We assume that shared passwords been generated as shown in
[read_and_write_passwords.md]({{< ref "./read_and_write_passwords.md" >}})


After changes of the roles or of the shared directories it is necessary to
reencrypt the shared passwords - otherwise, newly assigned subjects or roles
would not be able to read the passwords or removed roles would still be able.
This happens automatically.


## Changing Shares


Share a directory with the developer role


```bash
pass team share add my_team/some_dir/ developer
```

```bash_out
'my_team/some_dir/' is shared with: developer.
```

Insert a password


```bash
pass generate my_team/some_dir/some_password > /dev/null
```


Now add the manager role to the shared directory


```bash
pass team share add my_team/some_dir/ manager
```

```bash_out
'my_team/some_dir/' is shared with: developer, manager.
my_team/some_dir/some_password: reencrypting to 6D5DDAD70F134CE3 871C317AFCA78C44
```

And remove the developer role


```bash
pass team share remove my_team/some_dir/ developer
```

```bash_out
'my_team/some_dir/' is shared with: manager.
my_team/some_dir/some_password: reencrypting to 871C317AFCA78C44
```

## No Secret Key


However, if the agent does not have the read-permission for the shared
directory, the reencryption fails. We cannot just add the developer role
again:


```bash
pass team share set my_team/some_dir/ developer
```

```bash_out
'my_team/some_dir/' is shared with: developer.
Warning: Could not reencrypt 'my_team/some_dir/'.
         gpg: decryption failed: No secret key
```

The password store and the team is not in sync now. The team extension says the directory is shared with the developer role


```bash
pass team share show my_team/some_dir
```

```bash_out
Share: my_team/some_dir
├── Roles:
│   └── developer
└── GPG Keys:
    └── A4E73A7A7D316FD1 ── passt-user1
```

But gpg says it is encrypted for the `passt-user2` who doesn't have the developer role but the manager role.


```bash
gpg -d $PASSWORD_STORE_DIR/my_team/some_dir/some_password.gpg
```

```bash_err
gpg: encrypted with 3072-bit RSA key, ID 871C317AFCA78C44, created 2022-07-28
      "passt-user2"
gpg: decryption failed: No secret key

(exit 2)
```

This is easily fixed by reverting the operation


```bash
pass team share set my_team/some_dir/ manager
```

```bash_out
'my_team/some_dir/' is shared with: manager.
```

## Changing Roles


Everything that shown above also applies for changing the roles.


It is possible to assign a new `passt-user3` to the developer role. All shared passwords are reencrypted for `passt-user3`.


```bash
pass team role assign developer passt-user3
```

```bash_out
Assigned role 'developer' to subject 'passt-user3'.
'my_team/development' is shared with: developer, manager.
my_team/development/dev_password: reencrypting to 1538DC7CCFC72811 6D5DDAD70F134CE3 871C317AFCA78C44
```

However, it is not possible to add yourself to the manager role and reencrypt
the manager's passwords.


```bash
pass team role assign manager passt-user1
```

```bash_out
Assigned role 'manager' to subject 'passt-user1'.
'my_team/development' is shared with: developer, manager.

'my_team/management' is shared with: manager.
Warning: Could not reencrypt 'my_team/management'.
         gpg: decryption failed: No secret key
'my_team/some_dir' is shared with: manager.
Warning: Could not reencrypt 'my_team/some_dir'.
         gpg: decryption failed: No secret key
```

## How to Deal With 'No Secret Key'


It is necessary to have someone perform the changes who can read the
passwords. So adding someone to the manager role should be done by team members who have the manager role themselves. Sharing a directory with another role should also done by those who can already read it.


As we have seen above, both works well.


As another option, it is possible to initiate `pass team` with a `--trusted`
manager. A `--trusted` manager can read *all* passwords and thus they can also reencrypt passwords whenever this is necessary. Read more about the `--trusted` flag on the [Man Page]({{< ref "../docs/man-pass-team.md" >}})



*Generated from [reencryption.sh](https://gitlab.com/pass-team/pass-team/-/blob/0.1.0-rc.5/docs/examples/reencryption.sh)*
