---
title: Shared Password Directories
summary: "Example for sharing and unsharing directories, assigning roles to the shared directories and inspecting and listing shared directories. This example is build upon the [Roles Management](./roles_management/) example."
weight: 300
---



We assume that roles have been created as shown in
[roles_management.md]({{< ref "./roles_management.md" >}})


Create a shared directory for different purposes:


```bash
pass team share add my_team/development developer manager
```

```bash_out
'my_team/development' is shared with: developer, manager.
```

```bash
pass team share add my_team/management developer manager
```

```bash_out
'my_team/management' is shared with: developer, manager.
```

Using `set` instead of `add` removes all roles from the given share and adds
only the new ones.


```bash
pass team share set my_team/management manager
```

```bash_out
'my_team/management' is shared with: manager.
```

List all shares:


```bash
pass team share list
```

```bash_out
Shared Directories
└── my_team
    ├── development
    └── management
```

Show a particular share:


```bash
pass team share show my_team/management
```

```bash_out
Share: my_team/management
├── Roles:
│   └── manager
└── GPG Keys:
    └── 958C08EF59F46E8C ── passt-user2
```

Use `remove` to remove a particular role:


```bash
pass team share add my_team/developer-secrets developer manager
```

```bash_out
'my_team/developer-secrets' is shared with: developer, manager.
```

```bash
pass team share remove my_team/developer-secrets manager # ;)
```

```bash_out
'my_team/developer-secrets' is shared with: developer.
```

When `remove` removes the last role from a share, the directory is not shared anymore.


```bash
pass team share remove my_team/developer-secrets developer
```

```bash_out
'my_team/developer-secrets' is not shared any longer.
```

Use `unset` to remove a share entirely. This is equivalent to
`share remove <directory> --all`.


```bash
pass team share add some/wrong/path developer
```

```bash_out
'some/wrong/path' is shared with: developer.
```

```bash
pass team share unset some/wrong/path
```

```bash_out
'some/wrong/path' is not shared any longer.
```

Error handling:


```bash
pass team share show some/wrong/path
```

```bash_err
Error: Directory 'some/wrong/path' does not exist.

(exit 89)
```

```bash
pass team share unset some/wrong/path
```

```bash_err
Error: 'some/wrong/path' is not a shared directory.

(exit 91)
```


*Generated from [shared_password_directories.sh](https://gitlab.com/pass-team/pass-team/-/blob/0.1.0-rc.5/docs/examples/shared_password_directories.sh)*
