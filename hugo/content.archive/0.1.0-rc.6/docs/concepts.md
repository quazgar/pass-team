---
title: Concepts
summary: Central concepts of Role-based Access Management and how they are implemented in `pass team`.
---

`pass team` implements Role-based Access Control (RBAC) for `pass`.

The terms and abbreviations are oriented towards the
[Wikipedia article](https://en.wikipedia.org/wiki/Role-based_access_control)
on Role-based Access Control.

## Central Concepts

Subject (S)
: Every subject is identified by a gpg key. In many
cases 'subject' is just a fancy technical term for
'person', but there use cases where persons have several
gpg keys of course.

Role (R)
: Roles are created and managed via the `pass team role`
commands. Roles have a unique name (e.g.
`human-resources`).

Permission (P)
: There are only two notable permission in this
context---the permission to *read* a password in the
password store and the permission to *adminstrate* the
pass team roles and shares.

## Central Relations

Subject Assingment (SA = S x R)
: Roles are (un)assigned to subjects via the `pass
team role (un)assign` command.


Permission Assingment (PA = P x R)
: The permission to read the passwords in
a directory is assigned to a role by "sharing" that
directory with the role via `pass team share add ...`.
This results in the passwords being encrypted with all
gpg-ids which have been assigned the role in question.

Role Hierarchy (RH = R x R)
: *Not implemented yet*: Roles have a partial order,
i.e. a role can entail another role. E.g. the
"team-leader" role might entail the "team-member" role
and all passwords readable for the "team-member" are
also readable for the "team-leader".
