# Compile and Serve

The hugo pages are being compiled in the gitlab CI pipeline and deployed to
gitlab pages.

For developing and testing purposes run

```
docker run --rm -it -v $(pwd):/src -p 1313:1313 klakegg/hugo:0.105.0-ext server
```

in this directory.
