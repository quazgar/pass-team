randStr() {
    tr -dc A-Za-z0-9 < /dev/urandom | head -c "$1"
}

assertDirExists () {
    set -o noglob
    local directory
    directory="$(realpath "$1")"
    assertTrue "dir should exist: $directory" "[ -d '$directory' ]"
    set +o noglob
}

assertFileExists () {
    set -o noglob
    local file
    file="$(realpath "$1")"
    assertTrue "file should exist: $file" "[ -f '$file' ]"
    set +o noglob
}

assertFileNotExists () {
    set -o noglob
    local file="$1"
    assertTrue "file should not exist: $file" "[ ! -e '$file' ]"
    set +o noglob
}

assertFileEmpty () {
    set -o noglob
    local file
    file="$(realpath "$1")"
    assertFalse "file should be empty: $file" "[ -s '$file' ]"
    set +o noglob
}

assertFileNotEmpty () {
    set -o noglob
    local file
    file="$(realpath "$1")"
    assertTrue "file should not be empty: $file" "[ -s '$file' ]"
    set +o noglob
}

assertFileLinesEquals () {
    set -o noglob
    local file
    file="$(realpath "$1")"
    shift
    local expectedNextLine="$1"
    [[ -n "$expectedNextLine" ]] && assertFileNotEmpty "$file"
    [ "$#" -ne "0" ] && shift

    while IFS= read -r line; do
        assertFalse "file has more lines than expected: $file" \
            "[[ -z '$expectedNextLine' ]]"
        [[ -n "$expectedNextLine" ]] && \
            assertEquals "$expectedNextLine" "$line"
        expectedNextLine="${1-}";
        [ "$#" -ne "0" ] && shift
    done < "$file"
    set +o noglob
}

assertSetEquals () {
    local left left_array right right_array msg="set equals"
    if [ "$#" -eq "3" ] ; then
        msg="$1"
        shift
    fi
    readarray -d " " -t left_array < <(echo -n "$1")
    readarray -d " " -t right_array < <(echo -n "$2")
    left="$(printf '%s\n' "${left_array[@]}" | sort)"
    right="$(printf '%s\n' "${right_array[@]}" | sort)"
    assertEquals "$msg" "$left" "$right"
}
