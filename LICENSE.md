# License (GPLv3 or later)

`pass team` is an extension for
[pass - the standard unix password manager](https://www.passwordstore.org/).
`pass team` allows team members to share passwords among each other using
Role-based Access Control.

Copyright (C) 2021-2022 Timm C. Fitschen <t.fitschen@indiscale.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
[GNU General Public License](./GPLv3.md) for more details.
