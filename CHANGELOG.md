# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

### Added

* Basic Role-based Access Control
  * Initialize a team with team managers
    `pass team init <gpg-id>...`
    * normal team managers can manage roles and shares.
    * trusted team managers additionally have read-access to all passwords.
  * Manage roles (list/add/remove/assign/unassign).
    `pass team role ...`
  * Manage shares (list/add/remove/reencrypt)
    `pass team share`
  * Automatically reencrypt passwords after roles or shares have been changed.
  * Verbosity levels for debugging.
    `pass team -d[d]+ ...`
  * Quiet option for less verbose, machine-readable output
    `pass team -q ...`
  * Help
    `pass team --help`
* Man Page (`man pass-team`)
* Rudimentary bash-completion
* Installation via GNU Make
* Debian package
* Homebrew formula
* Support for [NO_COLOR](https://no-color.org/)

### Fixed

* [#2](https://gitlab.com/pass-team/pass-team/-/issues/2) - `pass team init
  --help` deinitializes the team

### Documentation

* Repository contains documentation and examples under [./docs/](./docs/)
* A Hugo-build, web-based documentation is now available at
  [pass-team.gitlab.io](https://pass-team.gitlab.io/).

<!--
## [Unreleased]

### Added
for new features.

### Changed
for changes in existing functionality.

### Deprecated
for soon-to-be removed features.

### Removed
for now removed features.

### Fixed
for any bug fixes.

### Security
in case of vulnerabilities.

### Documentation
for changes to the documentation
-->
