# Release Guidelines

The single source of truth for the releases are the git tags which must comply
with the requirements of [Semantic
Versioning](https://semver.org/spec/v2.0.0.html).

Take the very first release tag as an example: `0.1.0-rc.1"` -- not `Version
0.1.0-rc.1`, not `v0.1.0-rc.1`, just the version string.

There should be a [gitlab
release](https://gitlab.com/pass-team/pass-team/-/releases) for every git
tag, linking the documentation and pipeline-build packages.

## Preparation

1. Checkout `main` branch.
2. `OLD_VERSION="$(cat VERSION)"` -- we need this in a second.
3. Checkout `dev` branch which now contains the code which is to be released.
4. Create and checkout a `prepare-<VERSION>` branch.
5. Set the [VERSION file](./VERSION) to the new version (if not up-to-date).
6. Update [RELEASE](./RELEASE).
7. Update the [CHANGELOG.md](./CHANGELOG.md). If you are releasing
   a pre-release version. Just leave the `[UNRELEASED]` section. Everything
   should go in the normal release section as soon as the normal release is
   ready.
8. Update all generated files with `make`.
9. Update the `debian/changelog` file if necessary.
10.Build the web documentation content with `make pages` and the build the hugo
   pages as explained in `hugo/README.md`. Review the generated web
   documentation.
11. Review the output of `grep -i -r --exclude-dir=.git "$OLD_VERSION"` and
   `grep -i -r TODO`. Note: The `default_version` in the `hugo/config.toml` can
   remain at `$OLD_VERSION`. This way, the online documentation will show the
   newer version as a "Preview" version when the pages are being built and
   deployed in the prepare branch.
12.Review, commit and push the changes.

## Release

1. Given that all the tests are passing in the `prepare-<VERSION>` branch,
   merge the `prepare-<VERSION>` branch into `main` and remove the
   `prepare-<VERSION>` branch.
2. Update `hugo/config.toml` (e.g. `homepage_button_link` and
   `default_version`.
3. Given that all the tests are also passing in the `main` branch, go to
   gitlab.com and start a pipeline on main with "IS_RELEASE=TRUE". This will
   create a giblab release and tag the latest main branch commit

## After the Release

These steps initiate the next release cycle.

0.  In `main`, collect the download info
    via `eval "$(utils/collect_package_files.sh | grep "export")"`.
1.  Build the hugo contens
    via `make REFERENCE_BRANCH="$(cat VERSION)" clean-examples clean-pages examples pages`.
    We need the generated output for the archive.
2.  Switch to `dev` branch and merge the `main` branch into `dev`.
3.  Copy the (newly released) hugo content and data directories to the archive:
    `cp -r -t hugo/content.archive hugo/content/$(cat VERSION)/` and `cp -r -t
    hugo/data.archive hugo/data/$(cat VERSION)/`. This is necessary to
    have the web documentation for the older versions available in the future.
4.  Commit the `hugo/content.archive` and `hugo/data.archive`.
5.  `OLD_VERSION="$(cat VERSION)"` -- we need this in a second.
6.  Set the [VERSION file](./VERSION) to `dev`.
7.  Update [RELEASE](./RELEASE). Maybe set `RELEASE_DATE="$(date --rfc-3339=date)"`.
8.  Update all generated files with `make`.
9.  Update hugo stuff: `make clean-pages pages`.
10. Review the output of `grep -i -r --exclude-dir=.git "$OLD_VERSION"`. Some
    of the pipeline jobs will fail if there are old version strings where they
    should not.
11. Review, commit and push the changes.
