# Vision

`pass` is a wonderful piece of software. We want to use it in a team context.

It is the vision of this project to develop and maintain an extentions to help
teams to use `pass` for sharing passwords with their team members in
a convenient and secure way.

This leads to the following goals.

# Goals

1. Not all team members are allowed to read every password and so the passwords
   must be encrypted only for those who do.

2. It is desired to use `pass` with as little as possible additional logic or
   features most of the time. This way we can use all the amazing user
   interfaces from the `pass` community which do not support the `pass team`
   extension.

3. The management of the who-can-read-what must lay in the hands of privileged
   team members ('pass team managers').

4. The security and consistency of the team's passwords must be ensured and
   verifiable.

5. Accidental sharing passwords with the wrong people must be prevented.

6. When individuals leave the team, lose privileges or roles there must be
   a way to know which passwords are effected by that, which are thus possibly
   compromised and have to be updated to maintain the confidentiality.

7. We want to use git, other syncronization tools or shared file systems to
   distribute the team's passwords and to create back-ups. `pass team` must not
   get into the way of that.


